// console.log("Send Stars");


// [SECTION] OBJECTS
	// an object is a data type that is used to represent world objects.
	// it s a collection of related data and/or functionalities of method.
	// information stored in objects are represented in "key:value" pair
	// usually we call this as property
	// different data type may be stored in an object's property creating data structures.

// Creating ojects using object initializer/object literal notation;
	
	/*
		Syntax:
			let/const objectName = {
				keyA: valueA,
				keyB: valueB,
				. . .
			}

			// this creates/declares an object and also initializes/assigns its properties upon creation.
	*/

	
	let cellphone = {
		name: "Nokia 3210",
		manufactureDate: 1999,
		features: ['send message', 'make call', 'play games'],
		isWorking: true,
		owner: {
			fullName: "Chris",
			yearOfOwnage: 20
		}
	}

	console.log(cellphone);

		// Accessing/reading properties
			// Dot notation
			/*
				Syntax:
					objectName.propertyName;
			*/

		console.log("Accessing Using Dot Notation");
		console.log(cellphone.name);
		console.log(cellphone.isWorking);
		console.log(cellphone.features);
		console.log(cellphone.features[1]);

		console.log(cellphone.owner);
		console.log(cellphone.owner.fullName);
			// bracket notation
		console.log("Accessing Using Bracket Notation");
		console.log(cellphone["name"]);
		console.log(cellphone["owner"]["fullName"]);


		// Creating objects using constructor function

		/*
			- Creates a reusable function to create several objects that have the same data structure
			- This is useful for creating multiple instances/copies of an object
				- instance is a concrete occurence of any object which emphasize distinct/unique identity.

				Syntax:
					function objectName(valueA, valueB){
						this.propertyA = valueA,
						this.propertyB = valueB
					}
		*/

		// This is a constructor function
			function Laptop(name, manufactureDate, ram, isWorking){
				// this keyword allows us to assign object properties by associating them with the values received from a constructor function's parameter

				this.laptopName = name,
				this.laptopManufactureDate = manufactureDate,
				this.laptopRam = ram,
				this.isWorking = isWorking
			}

			// Instatiation
			// The "new" keyword creates an instance of an object
			let laptop = new Laptop('Lenovo', [2008], '2gb', true)
			console.log(laptop);

			let myLaptop = new Laptop('Macbook Air', 2020, '8gb', false)
			console.log(myLaptop);

			let oldLaptop = new Laptop("Portal R2E CCMC", 1980, "500 mb", false)
			console.log(oldLaptop);


			function Menu(mealName, mealPrice){
				this.menuName = mealName,
				this.menuPrice = mealPrice
			}

			let mealOne = new Menu('Breakfast', 299);
			console.log(mealOne);

			let mealTwo = new Menu('Dinner', 500);
			console.log(mealTwo);


		// Create Empty Objects
			let computer = {};
			let emptyObject = new Object();
			console.log(computer);
			console.log(emptyObject);


		// Access objects inside an array
			let array = [laptop, myLaptop];
			console.log(array);

			// get the value of the property laptopName in the first element of our object.
			console.log(array[0]['laptopName'])
			console.log(array[1].laptopManufactureDate)


// [SECTION] Initialization/Adding/Deleting/Reassigning Object Properties

	/*
		- Like any other varaible in JavaScript, object have their properties initialized or added after the object was created/declared
	*/

	let car = {}
	console.log(car);

		// Initialize/add ojbect property using dot notation

		/*
			Syntax
				objectName.propertyToBeAdded = value;
		*/

		car.name = "Honda Civic";
		console.log(car);


		// Initialize/add object property using bracket notation
		/*
			Syntax
				objectName["propertyToBeAdded"] = value;
		*/

		car['manufactureDate'] = 2019;
		console.log(car);


		// Deleting Ojbect Properties

			// deleting using dot notation
		/*
			Syntax
				delete objectName.propertyToBeDeleted;
		*/

		delete car.name;
		console.log(car);


			// deleting using bracket notation
		/*
			Syntax
				delete objectName["propertyToBeDeleted"];
		*/
		delete car["manufactureDate"];
		console.log(car);


		// Reassigning Object Properties
		car.name = "Honda Civic";
		car['manufactureDate'] = 2019;
		console.log(car);

			// reassigning object property using dot notation
				car.name = "Dodge Charger R/T"
				console.log(car);


			// reassigning object property using bracket notation
				car["name"] = "Jeepney"
				console.log(car);


		// [Reminder] if you will add property to the object make sure that the property name that you are going to add is not existing or else it will just reassign the value of the property.


// [SECTION] Object Methods
	// A methods is a function which is a property of an object
	// They are also function and one of the key differences they have is that methods are functions related to specific objects.

	let person = {
		name: "John Doe",
		talk: function(){
			console.log("Hello my name is " + this.name)
		}
	}

	console.log(person);

	person.talk();


	// Add method to object
	person.walk = function(){
		console.log(this.name + " walked 25 steps forward.")
	}

	console.log(person);
	person.walk();


	// Methods are useful for creating reusable function that perform tasks related to objects.

	// Constructor function with method

	function Pokemon(name, level){
		// Properties of the object
		this.pokemonName = name;
		this.pokemonLevel = level;
		this.pokemonHealth = 2 * level;
		this.pokemonAttack = level;

		// Method
		this.tackle = function(targetPokemon) {
			console.log(this.pokemonName + " used tackle on " + targetPokemon.pokemonName);
			let hpAftertackle = targetPokemon.pokemonHealth - this.pokemonAttack; 
			console.log(targetPokemon.pokemonName + " health is reduced by " + hpAftertackle)
		}


		this.faint = function(){
			console.log(this.pokemonName + " fainted!")
		}
	}

	let pikachu = new Pokemon('Pikachu', 12);
	console.log(pikachu);
	let gyarados = new Pokemon('Gyarados', 20);
	console.log(gyarados);

	pikachu.tackle(gyarados);

	pikachu.faint();